namespace Volte.Core.Models
{
    public enum ModActionType
    {
        Purge,
        Warn,
        ClearWarns,
        Delete,
        Kick,
        Softban,
        IdBan,
        Ban
    }
}
﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Discord;
using Gommon;
using Volte.Core;
using Volte.Core.Models;
using Volte.Core.Models.EventArgs;
using Color = System.Drawing.Color;
using Console = Colorful.Console;

namespace Volte.Services
{
    public sealed class LoggingService : VolteEventService
    {
        private readonly object _lock = new object();

        public override Task DoAsync(EventArgs args)
        {
            Log(args.Cast<LogEventArgs>());
            return Task.CompletedTask;
        }

        internal void Log(LogEventArgs args) =>
            Log(args.LogMessage.Internal.Severity, args.LogMessage.Internal.Source,
                args.LogMessage.Internal.Message, args.LogMessage.Internal.Exception);

        internal void PrintVersion() => Log(LogSeverity.Info, LogSource.Volte,
            $"Currently running Volte V{Version.FullVersion}.");

        private void Log(LogSeverity s, LogSource src, string message, Exception e = null)
        {
            lock (_lock)
            {
                if (s is LogSeverity.Debug)
                {
                    if (src is LogSource.Discord || src is LogSource.Gateway) { }

                    if (src is LogSource.Volte && !Config.EnableDebugLogging) return;
                }

                DoLogAsync(s, src, message, e);
            }
        }

        public void Debug(LogSource src, string message, Exception e = null) 
            => Log(LogSeverity.Debug, src, message, e);

        public void Info(LogSource src, string message, Exception e = null)
            => Log(LogSeverity.Info, src, message, e);

        public void Error(LogSource src, string message, Exception e = null)
            => Log(LogSeverity.Error, src, message, e);
        public void Critical(LogSource src, string message, Exception e = null)
            => Log(LogSeverity.Critical, src, message, e);

        public void Warn(LogSource src, string message, Exception e = null)
            => Log(LogSeverity.Warning, src, message, e);

        public void Verbose(LogSource src, string message, Exception e = null)
            => Log(LogSeverity.Verbose, src, message, e);

        public void LogException(Exception e)
            => Log(LogSeverity.Error, LogSource.Volte, string.Empty, e);

        private void DoLogAsync(LogSeverity s, LogSource src, string message, Exception e)
        {
            var (color, value) = VerifySeverity(s);
            Append($"{value} -> ", color);

            (color, value) = VerifySource(src);
            Append($"{value} -> ", color);

            if (!message.IsNullOrWhitespace())
                Append(message, Color.White);

            if (e != null)
                Append($"{Environment.NewLine}{e.Message}{Environment.NewLine}{e.StackTrace}", Color.IndianRed);

            Console.Write(Environment.NewLine);
        }

        private void Append(string m, Color c)
        {
            Console.ForegroundColor = c;
            Console.Write(m);
        }

        private (Color Color, string Source) VerifySource(LogSource source) =>
            source switch
                {
                LogSource.Discord => (Color.RoyalBlue, "DSCD"),
                LogSource.Gateway => (Color.RoyalBlue, "DSCD"),
                LogSource.Volte => (Color.Crimson, "CORE"),
                LogSource.Service => (Color.Gold, "SERV"),
                LogSource.Module => (Color.LimeGreen, "MDLE"),
                LogSource.Rest => (Color.Tomato, "REST"),
                LogSource.Unknown => (Color.Teal, "UNKN"),
                _ => throw new ArgumentNullException(nameof(source), "source cannot be null")
                };


        private (Color Color, string Level) VerifySeverity(LogSeverity severity) =>
            severity switch
                {
                LogSeverity.Critical => (Color.Maroon, "CRIT"),
                LogSeverity.Error => (Color.DarkRed, "EROR"),
                LogSeverity.Warning => (Color.Yellow, "WARN"),
                LogSeverity.Info => (Color.SpringGreen, "INFO"),
                LogSeverity.Verbose => (Color.Pink, "VRBS"),
                LogSeverity.Debug => (Color.SandyBrown, "DEBG"),
                _ => throw new ArgumentNullException(nameof(severity), "severity cannot be null")
                };
    }
}
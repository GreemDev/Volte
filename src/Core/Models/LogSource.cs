namespace Volte.Core.Models
{
    public enum LogSource
    {
        Module,
        Service,
        Discord,
        Rest,
        Gateway,
        Volte,
        Unknown
    }
}